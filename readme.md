# POC for `System.AddIn` development flow

This project is the result of following the tutorial at [Microsoft Docs](https://docs.microsoft.com/en-us/dotnet/framework/add-ins/walkthrough-create-extensible-app).  A notable difference is in how the host application runs, in which it:

* Loops over existing plugins
* Documents the name of the plugin to the console
* Executes the plugin with some hard-coded values
* Removes the AppDomain in which the plugin resides

In particular, the last step mentioned above allows for us to hot-swap the .dll files so that we can do "live" development against the executing host.
