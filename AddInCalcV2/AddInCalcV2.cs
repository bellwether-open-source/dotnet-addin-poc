﻿using CalcAddInViews;
using System;
using System.AddIn;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AddInCalcV2
{
	[AddIn("Calculator AddIn V2", Version = "2.0.0.0")]
	public class AddInCalcV2 : ICalculator
	{
		public double Add(double a, double b)
		{
			Console.WriteLine("Fake adding {0} and {1}", a, b);

			return -1;
		}

		public double Divide(double a, double b)
		{
			Console.WriteLine("Fake dividing {0} and {1}", a, b);

			return -1;
		}

		public double Multiply(double a, double b)
		{
			Console.WriteLine("Fake multiplying {0} and {1}", a, b);

			return -1;
		}

		public double Subtract(double a, double b)
		{
			Console.WriteLine("Fake subtracting {0} and {1}", a, b);

			return -3;
		}
	}
}
