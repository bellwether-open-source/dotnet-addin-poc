﻿using CalcAddInViews;
using CalculatorContracts;
using System;
using System.AddIn.Pipeline;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc1AddInSideAdapter
{
	[AddInAdapter()]
	public class CalculatorViewToContractAddInSideAdapter : ContractBase, ICalc1Contract
	{
		private ICalculator view;

		public CalculatorViewToContractAddInSideAdapter(ICalculator view)
		{
			this.view = view;
		}

		public double Add(double a, double b)
		{
			return view.Add(a, b);
		}

		public double Divide(double a, double b)
		{
			return view.Divide(a, b);
		}

		public double Multiply(double a, double b)
		{
			return view.Multiply(a, b);
		}

		public double Subtract(double a, double b)
		{
			return view.Subtract(a, b);
		}
	}
}
