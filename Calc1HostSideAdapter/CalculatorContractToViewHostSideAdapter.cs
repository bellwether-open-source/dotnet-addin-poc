﻿using Calc1HVA;
using CalculatorContracts;
using System;
using System.AddIn.Pipeline;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc1HostSideAdapter
{
	[HostAdapterAttribute]
    public class CalculatorContractToViewHostSideAdapter : ICalculator
    {
		private ICalc1Contract contract;
		private ContractHandle handle;

		public CalculatorContractToViewHostSideAdapter(ICalc1Contract contract)
		{
			this.contract = contract;
			this.handle = new ContractHandle(contract);
		}

		public double Add(double a, double b)
		{
			return contract.Add(a, b);
		}

		public double Divide(double a, double b)
		{
			return contract.Divide(a, b);
		}

		public double Multiply(double a, double b)
		{
			return contract.Multiply(a, b);
		}

		public double Subtract(double a, double b)
		{
			return contract.Subtract(a, b);
		}
	}
}
