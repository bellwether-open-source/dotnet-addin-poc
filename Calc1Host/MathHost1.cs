﻿using Calc1HVA;
using System;
using System.AddIn.Hosting;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc1Host
{
	class MathHost1
	{
		static void Main(string[] args)
		{
			String addInRoot = Environment.CurrentDirectory + "\\Pipeline";

			while (Console.ReadKey().Key != ConsoleKey.Escape)
			{
				AddInStore.Update(addInRoot);
				var tokens = AddInStore.FindAddIns(typeof(ICalculator), addInRoot);

				tokens.ToList().ForEach(token =>
				{
					Console.WriteLine();
					Console.WriteLine("Token name: {0}", token.Name);

					var calculator = token.Activate<ICalculator>(AddInSecurityLevel.Internet);

					Console.WriteLine("Add: 3 + 4 = {0}", calculator.Add(3, 4));
					Console.WriteLine("Subtract: 3 + 4 = {0}", calculator.Subtract(3, 4));
					Console.WriteLine("Multiply: 3 + 4 = {0}", calculator.Multiply(3, 4));
					Console.WriteLine("Divide: 3 + 4 = {0}", calculator.Divide(3, 4));

					var controller = AddInController.GetAddInController(calculator);
					AppDomain.Unload(controller.AppDomain);
				});
			}
		}
	}
}
